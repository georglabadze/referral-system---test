function hamburger(){
	var btn = $('#resBtn');

	btn.on('click', function(e){
		e.stopPropagation();
		$(this).toggleClass('active');
		$('.side-bar, .dash-content').toggleClass('active');
	});
	$('.side-bar').click(function(e){
		e.stopPropagation();
	})
	$('body, html').click(function(){
		btn.removeClass('active');
		$('.side-bar, .dash-content').removeClass('active');
	})
}



function calculator(){
	var btcCourse = 13711.60;
	var switchBtn = $('#switch-btn');
	var currencies = ['BTC', 'GEL'];

	var btcInput = $('input[data-currency="BTC"]'),
		gelInput = $('input[data-currency="GEL"]');
	
	function render(){
		$('.convert-element').each(function(i, item){
			$(item).find('.curr').html(currencies[i]);
			$(item).find('input').attr('data-currency', currencies[i]);
		});
	}

	function swap(){
		var btcVal = $('.convert-element input[data-currency="BTC"]').val();
		var gelVal = $('.convert-element input[data-currency="GEL"]').val();
		currencies.reverse();
		render();
		$('.convert-element input[data-currency="BTC"]').val(btcVal);
	    $('.convert-element input[data-currency="GEL"]').val(gelVal);
	}
	
	function listen(){
		$('.convert-element input').keyup(function(){
			var currency = $(this).attr('data-currency');
			var currency2 = currency == "BTC" ? "GEL" : "BTC";
			if(currency == "GEL"){
				var gelVal = $('.convert-element input[data-currency="GEL"]').val();
				$('.convert-element input[data-currency="BTC"]').val(gelVal / btcCourse);
			}else{
				var btcVal = $('.convert-element input[data-currency="BTC"]').val();
				$('.convert-element input[data-currency="GEL"]').val(btcVal * btcCourse );
			}
		});
		switchBtn.on('click', function(){
			swap();
		});
		$('.convert-element input').keypress(function(ev){
	    	var iKeyCode = (ev.which) ? ev.which : ev.keyCode;
	    	if(validateNumber(iKeyCode)){
	    		return true;
	    	}
	    	return false;
		});
	}

	function validateNumber(iKeyCode) {
	    if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
	        return false;
	    return true;
	}

	render();
	listen();
}

$(function(){
	hamburger();
	calculator();
});